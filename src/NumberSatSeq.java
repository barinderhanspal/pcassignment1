//******************************************************************************
//
// File:    NumberSatSeq.java
//
// This Java source file was developed using the features provided by the 
// Parallel Java 2 Library ("PJ2") (C) 2013 by Alan Kaminsky.
//
//******************************************************************************

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import edu.rit.pj2.LongVbl;
import edu.rit.pj2.Task;

/**
 * Class NumberSatSeq is a sequential program that solves the Number-SAT or #-SAT 
 * problem. In the Number-SAT/#-SAT problem, we determine to number of 
 * satisfying assignments for a given Boolean expression.
 * 
 * This boolean expression/formula is represented in CNF(conjunctive normal form)
 * which consists of <i>clauses</i> and <i>literals</i>. This program calculates
 * the number of satisfying assignments for a given input boolean expression
 * using a brute-force approach. The program then prints the count of satisfying
 * assignments to the console.
 * <P>
 * Usage: <TT>java pj2 NumberSatSeq <I>input_file_name<I></TT>
 * <P>
 * The program reads the input from the 'input_file_name' argument. The input 
 * must comply to the "DIMACS format". The program currently supports only 
 * variables in the range of [-63,63] excluding '0'(Zero). Also, the
 * number of clauses should be greater than or equal to 1.
 * 
 * @author Barinderpal Singh Hanspal (bxh5868@rit.edu)
 *
 */
public class NumberSatSeq extends Task{

	ArrayList<Long> clauses; // Array to store each of the clauses.
	
	/*
	 * Stores the flipping bitset information for working with NEGATIVE literals
	 * This flipping mask stores the bitset flipping(XOR operation) information 
	 * required to work with negative literals.
	 */
	ArrayList<Long> flippingMasks; 

	long numOfLiterals; // Number of Literals (V)
	long numOfClauses;  // Number of Clauses  (C)
	
	LongVbl numSatCount; // Counter to track number of satisfying assignments

	/**
	 * Constructor to initialize the class variables. Integer variables 
	 * numOfLiterals and numOfClauses are set to a default value of -1.
	 * numSatCount variable is set to 0. Other variables are created using
	 *  the 'new' keyword.
	 */
	public NumberSatSeq(){
		
		numOfLiterals = -1;
		numOfClauses = -1;
		
		clauses = new ArrayList<Long>();
		flippingMasks = new ArrayList<Long>();		
		
		// Initializing numSatCount to '0'(zero)
		numSatCount = new LongVbl(0);
	}

	/**
	 * Main function of the program.
	 * @param args arguments read from the command line
	 * @throws Exception throws Exception, FileNotFoundException, IllegalArgumentException, etc.
	 * 					  when it encounters issues reading the input file.
	 */
	public void main(String[] args) throws Exception {

		// Validate command line arguments
		if (args.length != 1) 
			usage();

		File inputFile = new File(args[0]);

		// Extract the CNF expression from the input file
		extractCnfExpression(inputFile);
	

		// Print the value of NumSAT returned by the calculateNumSAT() function
		System.out.println(calculateNumSAT().item);
	}

	
	/**
	 * The calculateNumSAT function, calculates the number of satisfying 
	 * assignments for the CNF expression read from the input file. The 
	 * function uses brute-force to calculate the solution. A for loop iterates
	 * over all the subsets of elements 0 (zero) through ('numOfLiterals' - 1)
	 * and for each of the subsets, the values of respective bit indexes are 
	 * substituted into the respective literals in the input CNF expression.
	 * If the complete CNF expression is satisfied(returns 'true'), the counter
	 * variable (numSatCount) keeping track of the number of satisfying 
	 * assignments is incremented by one.
	 * 
	 * @return LongVbl value representing the number of satisfying assignments
	 * 				   for the CNF expression read from the input file.
	 */
	private LongVbl calculateNumSAT() {

		// Calculating the last subset for the given value of number of literals
		long maxAssignment = (1L << numOfLiterals) - 1L;
		
		for (long assignmentIndex = 0L; assignmentIndex <= maxAssignment; 
				++ assignmentIndex){

			// Initializing the cnfValueForCurrentSubset variable to 'true'
			boolean cnfValueForCurrentSubset = true;

			for(int cnfClauseIndex = 0; cnfClauseIndex < clauses.size(); cnfClauseIndex ++){
				

				/*
				 * A clause value for the current cnf expressiom is calculated as:
				 * 1. XOR/flip the assignmentIndex long integer bitset using the flipping mask
				 * 		for the given clause.
				 * 2. Check if the long integer bitset value exists in the result from step 1.
				 */
				boolean clauseValueForCurrentSubset =	( ((assignmentIndex ^ flippingMasks.get(cnfClauseIndex)) & clauses.get(cnfClauseIndex) ) != 0L);
				

				if(clauseValueForCurrentSubset == false){
					
					// If a clause value is calculated as 'false', set cnf value as 'false' and break.
					cnfValueForCurrentSubset = false;
					break;
				}
				
				// Perform an AND operation to calulate result of conjunction of two or more clauses.
				cnfValueForCurrentSubset &= clauseValueForCurrentSubset;
					
			}

			if(cnfValueForCurrentSubset == true){
				/*
				 *  If the CNF expression for the given subset assignment evaluates to true
				 *  increment numSatCount
				 */
				++ numSatCount.item;
			}
		}

		// Return value of calculated NumSAT for input CNF expression
		return numSatCount;		
	}


	/**
	 * Print a usage message and exit program when invalid input arguments
	 * are provided.
	 * 
	 * @throws IllegalArgumentException throws an illegal argument expression exception
	 * 									when number of args do not match requirements
	 */
	private void usage() {

		System.err.println("Usage: java pj2 NumberSatSeq <input_file>");
		throw new IllegalArgumentException();
	}

	/**
	 * This function is used to extract the given CNF expression from the input file.
	 * The CNF expression must be follow the "DIMACS format" for SAT solvers. The
	 * function uses Scanners on the input file to extract the clauses and literals.
	 * The program also checks if the value for number of literals (V) lies between
	 * the range [1,63] and also if the number of clauses (C) has a value greater
	 * than 1. A check is also performed on the values of literals to ensure that
	 * they meet the range requirements(positive and negative)
	 * 
	 * @param inputFile the file from which the input CNF expression is to be 
	 * 					extracted.
	 * @throws Exception throws Exception when input does not comply to rules.
	 */
	private void extractCnfExpression(File inputFile) throws Exception{	

		Scanner fileScanner = new Scanner(inputFile);
		
		/*
		 * The flipping mask contains bitsets with information about which of the index
		 * of the assignments needs its values to be flipped (to represent a negation operation).
		 * Flipping masks for each and every clause are stored in its corresponding clause index
		 * of flippingMasks arraylist. 
		 */
		long flippingMask = 0L;
		long clause = 0L;
					
		while(fileScanner.hasNext()){

			if(fileScanner.hasNextLong()){

				long readValue = fileScanner.nextLong();

				if(numOfLiterals == -1){

					// If numOfLiterals variable is not initialized with data from file
					
					if(readValue < 1) {

						/*
						 *  If value read from file is less than 1, close fileScanner to
						 * avoid resource leaks and throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("Number of literals(V) cannot be less than 1.");

					} else if(readValue > 63) {

						/*
						 *  If value read from file is greater than 63, close fileScanner to
						 * avoid resource leaks and throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("Number of literals(V) cannot be greater than 63.");
					}

					// Valid input value
					numOfLiterals = readValue;

				} else if (numOfClauses == -1){

					// If numOfClauses variable is not initialized with data from file
					
					if(readValue < 1){

						/*
						 * If value read from file is less than 1, close fileScanner to
						 * avoid resource leaks and throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("Number of clauses(C) cannot be less than 1");
					}

					// Valid input value
					numOfClauses = readValue;

				} else {

					// Read Clauses and Literals

					if(Math.abs(readValue) > numOfLiterals){
						
						/*
						 * If literal value exceeds the max literal value mentioned in input 
						 * configuration close the fileScanner to avoid resource leaks and 
						 * throw exception indicating invalid input
						 */
						fileScanner.close();
						throw new Exception("One or more clauses in the input CNF expression"
								+" have a value of literal(V) greater than defined value");
						
					} else if(readValue == 0){
						
						/*
						 * If a value of '0' is read, signifying end of clause, add the literals
						 * read till this point into the array list storing the clauses. Also, add
						 * the corresponding flipping masks to handle negative literal values
						 */
						clauses.add(clause);
						flippingMasks.add(flippingMask);
						
						// Resetting the flipping mask and clause long integer bitset variables
						flippingMask = 0L;
						clause = 0L;
					
					} else {

						if(readValue < 0){
						
							/* 
							 * Set flipping bit in the flippingMask long integer bitset variable
							 * for the negative literal value.
							 */
							flippingMask |= 1L << (Math.abs(readValue) - 1);
						}
						
						clause |= 1L << (Math.abs(readValue) - 1);
					}
				}
				
			} else {
				
				// Skip characters(non Long values)
				fileScanner.next();
			}
		}
		
		fileScanner.close();
		
		if(numOfClauses != clauses.size()){
			
			/*
			 * If number of clauses read in the expression do not match the declared value
			 * in the input configuration, throw an exception indicating invalid input
			 */
			throw new Exception("Number of clauses(C) in the CNF expression read from file does not match the defined value");
		}
	}
}

